# Fichier de déploiement de l'application :
#
#  - Définit le nombre d'instance d'application (pod) via la propriété "replicas"
#
#  - Définit l'image Docker privée archivée sur GitLab pour instancié le conteneur du pod
#    (définition via un template)
#
#  - référence un fichier "ConfigMap" pour fournir au conteneur instancié un ensemble
#    de variables d'environnements (utilisé par l'application Django par chargement
#    dans le fichiers settings.py (via la fonction "os.environ.get").
#    NB : ce fichier est généré à la volée par le script python "configmap.py".

apiVersion: apps/v1
kind: Deployment
metadata:
  name: django-meteo-app-deployment
  labels:
    app: django-meteo-app-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: django-meteo-app-deployment
  template:
    metadata:
      labels:
        app: django-meteo-app-deployment
    spec:
      # Utilisation du secret kubernetes (déjà déployé) pour
      # s'authentifier auprès de de GitLab afin de récupérer
      # l'image Docker dans le conteneur privé.
      imagePullSecrets:
        - name: registry-credentials
      containers:
        # ---------------------------------------------------------------------------
        # conteneur de l'application instancié à partir de l'image GitLab
        # ---------------------------------------------------------------------------
        - name: django-meteo-container
          image: registry.gitlab.com/aldevpython/djangometeo:latest
          imagePullPolicy: Always
          ports:
            - containerPort: 8000
              name: gunicorn
          env:
            - name: POSTGRES_APPLICATION_DB
              valueFrom:
                secretKeyRef:
                  name: cloudsql-credentials
                  key: database
            - name: POSTGRES_APPLICATION_USER
              valueFrom:
                secretKeyRef:
                  name: cloudsql-credentials
                  key: username
            - name: POSTGRES_APPLICATION_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: cloudsql-credentials
                  key: password
            - name: POSTGRES_APPLICATION_HOST
              valueFrom:
                secretKeyRef:
                  name: cloudsql-credentials
                  key: host
            - name: POSTGRES_APPLICATION_PORT
              valueFrom:
                secretKeyRef:
                  name: cloudsql-credentials
                  key: port
            - name: PRODUCTION
              value: "TRUE"
            - name: GS_BUCKET_NAME
              value: "django-meteo-bucket"
            - name: DEBUG
              value: "False"
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP

        # ---------------------------------------------------------------------------
        # conteneur "proxy" nécessaire pour communiquer avec l'instance postgresql
        #
        # NB : bien que considéré comme un conteneur séparé du conteneur exécutant
        # l'application, les ressources réseaux sont mutualiséés de telle sorte
        # que pour uiliser le conteneur "proxy sql" le serveur d'application
        # utilise l'adresse de la boucle locale, comme si le serveur SQL était
        # sur la même machine.
        #
        # Le serveur proxy redirige ensuite le traffic vers
        # l'instance du serveur SQL Distant.
        #
        # En pratique, un seul et même pod kubernetes est partagé par deux conteneurs,
        # c'est pour cela que l'interface réseau est "commune".
        #
        # Ce mécanisme est appelé "sidecar"
        #
        # Pour plus de détails sur ce qu'apporte ce mécanisme "sidecar" dans le cas
        # d'une utilisation d'un proxx sql, se référener à la documentation :
        # https://cloud.google.com/sql/docs/postgres/connect-kubernetes-engine
        #
        # S'assurer d'utiliser la dernière version de l'image du "Cloud SQL Proxy" :
        # https://github.com/GoogleCloudPlatform/cloud-sql-proxy/releases
        #
        # /!\
        # ATTENTION : utiliser la chaine de connexion à l'instance SQL propre
        # à votre projet, et remplacer celle existante présente à la ligne "command"
        # /!\
        # ---------------------------------------------------------------------------
        - name: cloudsql-proxy
          image: gcr.io/cloudsql-docker/gce-proxy:latest
          command: [ "/cloud_sql_proxy",
            "-instances=abiding-team-379510:europe-west9:postgresql-instance-prod=tcp:5432",
            "-credential_file=/secrets/cloudsql/cloudsql-oauth-credentials.json" ]
          volumeMounts:
            - name: secrets-volume
              mountPath: /secrets/cloudsql
              readOnly: true
      volumes:
        - name: secrets-volume
          secret:
            secretName: cloudsql-oauth-credentials